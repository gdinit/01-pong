# PONG
### A clone of the Classic Game Pong by gdinit
***This Software is in pre-release phase!***

Intro
--------------
Everybody knows Pong, right?

Screenshots
--------------
Here is two!

<img src="extras/github_readme_screenshots/title.png" height="512" alt="TitleScreenshot"/>
<img src="extras/github_readme_screenshots/gameplay.png" height="512" alt="GameplayScreenshot"/> 

Install
-------
There is a pre-release Windows binary available under releases.

Credits
-------
Please refer to "documentation/".

License
-------
See the additional LICENSE file, under "documentation/".
